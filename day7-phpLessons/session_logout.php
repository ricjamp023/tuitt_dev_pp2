<?php 
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php  
		//remove all session variables
		session_unset();
		//destroy session
		session_destroy(); 
		header("refresh:2;url = http://localhost/day7/twitterSample.php"); 
	?>
	<h2>Logged out successfully. Redirecting to login page.</h2>


</body>
</html>