<?php 
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sample Twitter Login Page</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<style type="text/css">

	#mainNav {
		margin-bottom: 0;
		border-bottom: 1px solid rgba(0,0,0,0.2);
		border-radius: 0;
	}

	#leftHead {
		margin-left: 150px;
	}

	#rightMoved {
		margin-right: 150px;
	}

	.jumbotron {
		padding-top: 15px;
		height: 1000px;
	}

	.jumbotron .container {
		background-color: white;
		max-width: 950px;
		border: 1px solid rgba(0,0,0,0.2);
		border-bottom: 1px solid rgba(0,0,0,0.1);
	}

	.jumbotron .jumboBottom {
		border: 1px solid rgba(0,0,0,0.2);
		border-top: none;
		background-color: rgba(211,211,211,0.1);
	}

	.container h2,h5 {
		margin-left: 150px;
	}

	#loginForm {
		margin-left: 150px;
		margin-bottom: 30px;
	}

	input[type='text'], input[type='password'] {
		min-width: 300px;
		margin-left: 0;
	}

	input[type='password'] {
		margin-bottom: 10px;
	}

	input::-webkit-input-placeholder {
	color: black !important;
	}
	 
	input:-moz-placeholder { /* Firefox 18- */
	color: black !important;  
	}
	 
	input::-moz-placeholder {  /* Firefox 19+ */
	color: black !important;  
	}
	 
	input:-ms-input-placeholder {  
	color: black !important;  
	}

	.btn-primary {
		border-radius: 999px;
	}

	span {
		font-size: 0.7em;
	}

	h5.incorrect {
		margin-left: auto;
		color: red;
	}

	</style>

</head>
<body>

	<nav id="mainNav" class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div id="leftHead" class="navbar-header">
	      <a class="navbar-brand" href="#">Bird</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="#">Home</a></li>
	        <li><a href="#">About</a></li>
	      </ul>
	      <ul id="rightMoved" class="nav navbar-nav navbar-right">
	        <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Language: <strong>English</strong> <span class="caret"></span></a>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="jumbotron">
		<div class="container">
			<h2>Log in to Twitter</h2>
			<form id="loginForm" action="session_home.php" method="POST">
				<div class="form-group">
					<input type="text" placeholder="Phone, email or username" name="userName">
				</div>
				<div class="form-group">
					<input type="password" placeholder="Password" name="userPass">
				</div>
				<?php
					if(isset($_SESSION['login']) && !$_SESSION['login']) {
					    echo "<h5 class='incorrect'>Please input your correct credentials.</h5>";
					    session_unset();
					    session_destroy();
					}
				?>
				<button type="submit" class="btn btn-primary">Log in</button>
				<label><input type="checkbox"> Remember me * <a href="#">Forgot password?</a></label>
			</form>
		</div>
		<div class="jumboBottom container">
			<h5>New to Twitter? <a href="#">Sign up now <span>&gt;&gt;</span></a></h5>
			<h5>Already using Twitter via text message? <a href="#">Activate your account <span>&gt;&gt;</span></a></h5>
		</div>
	</div>

</body>
</html>