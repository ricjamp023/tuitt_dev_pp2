<!DOCTYPE html>
<html>
<head>
	<title>Calculate Perimeters & Area</title>
</head>
<body>

<?php 
//make a fnc that calculates the perimeter of any polygon
//square = s4, rectangle = 2l+2w, triangle = a+b+c

function perimeter($value) {
	if(sizeof($value)==1) 
		return ($value[0] * 4);
	elseif(sizeof($value)==2) 
		return ($value[0]*2) + ($value[1]*2);
	elseif(sizeof($value)==3) 
		return ($value[0]) + ($value[1]) + ($value[2]);
}

// square
// $shape = [5];
//rectangle
// $shape = [5,2];
// triangle
$shape = [5,1,3];

echo perimeter($shape);

echo "<br>";

date_default_timezone_set("Hongkong");
echo date('d-m-Y h:i:s A');

?>

</body>
</html>