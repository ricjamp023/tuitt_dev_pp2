<!DOCTYPE html>
<html>
<head>
	<title>Bootstrap Navbar Styled</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">

		.header, footer {
			text-align:center;
		}

		footer {
			background-color: skyblue;
		}

		.navbar-inverse {
			border-radius: 0;
		}

	</style>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>

	<?php

		echo "<nav class='navbar navbar-inverse'>
			  	<div class=container-fluid>
			  		<div class=navbar-header>
			  			<a class=navbar-brand href=#>WebSiteName</a>
			  		</div>
				    <ul class='nav navbar-nav navbar-right'>
					    <li class=active><a href=#>Home</a></li>
					    <li><a href=#>Page 1</a></li>
					    <li><a href=#>Page 2</a></li>
					    <li><a href=#>Page 3</a></li>
				    </ul>
		   		</div>
			  </nav>";

	?>

</body>
</html>
