<!DOCTYPE html>
<html>
<head>
	<title>PHP: JSON Objs</title>
</head>
<body>

	<!-- <p id="demo"></p>

	<script type="text/javascript">
		var person = {"name":"jam", "age":25,"gender":"male","cats":["Patches","Belly","Marco","Polo","Whiskers","Runt"]};
		var myJSON = JSON.stringify(person);
		document.getElementById('demo').innerHTML = myJSON;
	</script>
 -->

 <?php
	 if(!isset($myObj))
	 	$myObj = new stdClass();

	 $myObj->name = "John";
	 $myObj->age = 30;
	 $myObj->city = "New York";
	 $myObj->success = false;

	 $myJSON = json_encode($myObj);

	 echo $myJSON;
 ?>

</body>
</html>