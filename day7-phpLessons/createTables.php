<!DOCTYPE html>
<html>
<head>
	<title>Create a table</title>
</head>
<body>

<?php

 // given a row and column argument

function createTable($rw,$cl) {
	//parameters are row and col, respectively

	echo "<table border=1>";

	for($i = 0; $i < $rw; $i++) {
		echo "<tr>";
		for($j = 0; $j < $cl; $j++) {
			echo "<td>Content</td>";
		}
		echo "</tr>";
	}

	echo "</table>";

}

createTable(5,5);

?>


</body>
</html>