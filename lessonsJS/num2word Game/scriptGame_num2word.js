//automate it with a button click
var counterAll = 0;

$(document).ready(function(){
	//show preset/default value on load
	num2wordMain();		
	//button mode clicked, default gameMode of 100
	var gameMode = 100;
	$("#ezMode").click(function(){
		gameMode = 200;
	});
	$("#normMode").click(function(){
		gameMode = 100;
	});
	$("#hardMode").click(function(){
		gameMode = 50;
	});
	$("#uhardMode").click(function(){
		gameMode = 10;
	});
	$("#shardMode").click(function(){
		gameMode = 1;
	});
	var automateUp;
	$("#faceIt").click(function(){
		$(".challenged").hide();
		var challengerName = prompt("What is your name, Challenger?");
		$("#chalName").val(challengerName);
		$(".challenger").show();			
		automateUp = setInterval(zero2Ever,gameMode);
	});
	$("#switch").click(function(){
		if(counterAll%10 == 0) {
			alert("Yey, you did it!");
			amaWinner();
			} else {
				alert("Too bad, try again!");
				urALoser();
				}
		$("#digNum").val(counterAll=0);
	});
	$("#resetSwitch").click(function(){
		//reset values
		gameMode = 100;
		counterAll = 0;
		$("#digNum").val(0);
		$("#wrdNum").html("zero");
		$(".challenged").show();
		$(".challenger").hide();
		// $("#loserList li").remove();
		// $("#winnerList li").remove();
		clearInterval(automateUp);
	});

	function pressSpace(event) {
		var uniPress = event.which || event.keyCode;
		if(uniPress == 32) {
			if(counterAll%10 == 0) {
				alert("Yey, you did it!");
				amaWinner();
				} else {
					alert("Too bad, try again!");
					urALoser();
					}
			$("#digNum").val(counterAll=0);
		}
	}
});

//function to increase input value per interval
function zero2Ever() {
		counterAll++;
		$("#digNum").val(counterAll);
		num2wordMain();
}

//if value ever changes manually
$("#digNum").change(function(){
	num2wordMain();
});

//ordered list grabs names
	function urALoser() {
		var listItem = document.createElement("LI");
		var loserName = ($("#chalName").val()) + " - " + ($("#digNum").val());
		var loserNameNode = document.createTextNode(loserName);
		listItem.appendChild(loserNameNode);
		document.getElementById('loserList').appendChild(listItem);
	}

	function amaWinner() {
		var listItem = document.createElement("LI");
		var winnerName = ($("#chalName").val()) + " - " + ($("#digNum").val());
		var winnerNameNode = document.createTextNode(winnerName);
		listItem.appendChild(winnerNameNode);
		document.getElementById('winnerList').appendChild(listItem);
	}