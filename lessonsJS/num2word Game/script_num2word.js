//global variables for array containing word equivalents
//start with one digit
var single = ["zero","one","two","three","four","five","six","seven","eight","nine"];
//then two digits
var double = ["ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen", "nineteen"];
//multiples of ten
var doubleTens = ["twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"];
var triple = "hundred";
var quad = "thousand";

function num2wordMain() {
	//clear previous value
	$("#wrdNum").html("");
	//function reads input field value and outputs its word form
	var num = Number($("#digNum").val());
	var length = (num.toString()).length;
	//use standard array length counter
	//check length for thousand, hundred, or less
	if(length == 1){num2wordSingle(num);} 
	else if(length == 2){twoDigits(num);}
	else if(length == 3){threeDigits(num);}
	else if(length == 4){fourDigits(num);}
}

function num2wordSingle(num) {
	//loop through "single" array checking values
	for(var i = 0; i < single.length; i++) {
		if(Number(num) == i) {
			$("#wrdNum").append(single[i]);
			break;
		}
	}
}

function twoDigits(num) {
	//<20 use teen array
	if(Number(num)<20) {
			num2wordDouble(num);
	} else {
			//loop through first digit
			if(num%10 != 0) {
				//get the multiple of 10 only
				var y = num-(num%10);
				num2wordDoubleTens(y);
				//add space between words
				$("#wrdNum").append(" ");
				//get the single digit
				var z = num%10;
				num2wordSingle(z);
			} else {
					//loop through "doubleTens" array
						num2wordDoubleTens(num);
					}
		}
	} 

function num2wordDouble(num) {
	//loop through "double" array checking values
	for(var i = 0; i<double.length; i++) {
		if(Number(num) == i+10) {
			$("#wrdNum").append(double[i]);
			break;
		}
	}
}

function num2wordDoubleTens(num) {
	//loop through "doubleTens" array checking values
	for(var i = 0, j = 20; i < doubleTens.length; i++) {
		if(Number(num) == j) {
			$("#wrdNum").append(doubleTens[i]);
			break;
		}
		j=j+10;
	}
}

function threeDigits(num) {
	//if divisible by 10, just get first digit
	if(num % 10 == 0) {
		num2wordSingle(parseInt(num/100));
		$("#wrdNum").append(" ");
		$("#wrdNum").append(triple);
	} else { //go through first digit to end
		num2wordSingle(parseInt(num/100));
		$("#wrdNum").append(" ");
		$("#wrdNum").append(triple);
		$("#wrdNum").append(" ");
		//pass remaining to single and double arrays
		var y = num%100;
		//could it be <=10?
		num2wordSingle(y);
		//most likely >10
		twoDigits(y);
	}
}

function fourDigits(num) {
	if(num % 1000 == 0) {
		num2wordSingle(parseInt(num/1000));
		$("#wrdNum").append(" ");
		$("#wrdNum").append(quad);
	} else { //go through first digit to end
		num2wordSingle(parseInt(num/1000));
		$("#wrdNum").append(" ");
		$("#wrdNum").append(quad);
		$("#wrdNum").append(" ");
		//pass remaining to single and double arrays
		var y = num%1000;
		threeDigits(y);
	}
}