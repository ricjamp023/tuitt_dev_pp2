function popLyrics() {

//clear the container for multiple button presses
document.getElementById('xmasLyrics').innerHTML = "";

var lyrics = ["a Partridge in a pear tree.","Two turtle doves", "Three french hens", "Four colly birds",
"Five gold rings", "Six geese a-laying", "Seven swans a-swimming", "Eight maids a-milking", 
"Nine ladies dancing", "Ten lords a-leaping", "Eleven pipers piping", "Twelve drummers drumming"];

//just make those long strings into vars
var startVerse = "On the ";
var midVerse =  " day of Xmas<br> my true love sent to me: <br>";

	//2 levels of loops, 1st loop outer counter, 2nd loop repeater of previous statements
	for(var i=0; i<lyrics.length; i++) {
		//if at first
		if(i == 0) {
			document.getElementById('xmasLyrics').innerHTML += startVerse + "1st" + midVerse + lyrics[i];
		}
		//if at second
		else if(i == 1) {
			document.getElementById('xmasLyrics').innerHTML += startVerse + "2nd" + midVerse + lyrics[i] + ",<br>";
		}
		else if(i == 2) {
			document.getElementById('xmasLyrics').innerHTML += startVerse + "3rd" + midVerse + lyrics[i] + ",<br>";
		}
		else {
			document.getElementById('xmasLyrics').innerHTML += startVerse + (i+1) + "th" + midVerse + lyrics[i] + ",<br>";
		}
		for(var j=i; j>0; j--) {
			//add "and" if at last index
			if (j==1) {
				document.getElementById('xmasLyrics').innerHTML += " and " + lyrics[j-1];
			} else {
				document.getElementById('xmasLyrics').innerHTML += " " + lyrics[j-1] + ",<br>";
				}
		}
		document.getElementById('xmasLyrics').innerHTML += "<br><br>";
	}


}