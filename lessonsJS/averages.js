function batchJumbo() {
	var classList = [
	"Carlo Barcena",
	"Jango Agustin",
	"Louie Naraja",
	"Kristal Rose Corona",
	"Angela Frielle Belicena",
	"Ramir Fortunato",
	"Ira Rose Gayle Balajadia",
	"Krizzia Venci Mocling",
	"Juhnar Cabinian Esmeralda",
	"Timothy Clarence A. Cruz",
	"Nanaho Fujino",
	"Rodney Cunanan",
	"Jessica Salimbacod",
	"Rita Canon",
	"Chino Daquigan",
	"Nyssa Michelle D. Kho"
	];

	var studentsList = document.getElementsByClassName('student');
	//loop through studentsList to randomize each value
	for(var i = 0; i<studentsList.length; i++) {
		studentsList[i].value = classList[Math.floor(Math.random()*classList.length)];
	}
}


function randoScores() {
	document.getElementById('ohCrap').style.display = "block";
	//Exam 1
	var mixedOneArr = document.getElementsByClassName('score1');
	randomizeEm(mixedOneArr);
	//Exam 2
	var mixedTwoArr = document.getElementsByClassName('score2');
	randomizeEm(mixedTwoArr);
	//Exam 3
	var mixedThreeArr = document.getElementsByClassName('score3');
	randomizeEm(mixedThreeArr);
	//Exam 4
	var mixedFourArr = document.getElementsByClassName('score4');
	randomizeEm(mixedFourArr);
}

function randomizeEm(arr) {
	//accepts an array, then modify each index
	for(var i = 0; i < arr.length; i++) {
		// arr[i].value = ((Math.random() * 100) + 1).toFixed(2);
		arr[i].value = (Math.random() * (100-75) + 75).toFixed(2);
	}
}

function clearDef() {
	//get all input tags
	var clearInpArr = document.getElementsByTagName("INPUT");
	//loop through each input and remove their values
	for(var i = 0; i < clearInpArr.length; i++) {
		clearInpArr[i].value = "";
	}
	var clearPerArr = document.getElementsByClassName("personalAve");
	//loop through each personal average and remove their values
	for(var i = 0; i < clearPerArr.length; i++) {
		clearPerArr[i].innerHTML = "";
	}
	var clearTotArr = document.getElementsByClassName("computedAve");
	//loop through each exam average and remove their values
	for(var i = 0; i < clearTotArr.length; i++) {
		clearTotArr[i].innerHTML = "";
	}
	//remove totalComputedAve value
	document.getElementById('totalComputedAve').innerHTML = "";
}


function getTotalAverage() {
	//get all scores through a class called score and place it in an array
	var score = "", computed = "", scoreAve = 0, trimScoreAve = 0;
	for(var index = 1; index <=4; index++) {
		//Exam 1
		score = "score" + index;
		scoreAve = computeAverage(document.getElementsByClassName(score));
		trimScoreAve = scoreAve.toFixed(2);
		document.getElementsByClassName('computedAve')[index-1].innerHTML = trimScoreAve;
	}
	//Total Exam Average
	var totalAveArr = document.getElementsByClassName('computedAve');
	//for loop through array to get the sum
	for(var i = 0, sum = 0; i < totalAveArr.length; i++) {
		sum += Number(totalAveArr[i].innerHTML);
	}
	var totalAve = sum/(totalAveArr.length);
	var trimTotalAve = totalAve.toFixed(2);
	document.getElementById('totalComputedAve').innerHTML = trimTotalAve;
}

function computeAverage(arr) {
	//as you go through the array, have a temporary sum, 
	for(var i = 0, sum = 0; i < arr.length; i++) {
		sum += Number(arr[i].value);
	}
	//once you've traversed the array, 
	//then get average mean by dividing by (array.length+1)
	return (sum/arr.length);
}

function getPersonalAve() {
	var student = "", scoreAve = 0;
	var personalArr = document.getElementsByClassName('personalAve');
	for(var i = 1; i<=10; i++) {
		student = "student" + i;
		scoreAve = computeAverage(document.getElementsByClassName(student));
		trimScoreAve = scoreAve.toFixed(2);
		personalArr[i-1].innerHTML = trimScoreAve;
	}
}





