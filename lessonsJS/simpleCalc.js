
	//accepts the button value (0-9) as a parameter
	function getNums(num) {
		var atNum = document.getElementById('currNum').innerHTML;
		if(atNum == 1){
			var firstNum = document.getElementById('numOne').innerHTML;		
			//check first if number is empty
			if(firstNum == false) {
				firstNum = num;
			} else {
				//prevent multiple decimal points into one number
					if(num == '.') { //if num is a decimal point
						//and if your number already has one, do nothing
						if((firstNum.indexOf('.')) != -1) {
						}
						else {firstNum += num;}
					} else {firstNum += num;}
				}
			document.getElementById('numOne').innerHTML = firstNum;	
		}
		else {
			var secondNum = document.getElementById('numTwo').innerHTML;
			if(secondNum == false) {
				secondNum = num;
			} else {
				//prevent multiple decimal points into one number
					if(num == '.') { //if num is a decimal point
						//and if your number already has one, do nothing
						if((secondNum.indexOf('.')) != -1) {
						}
						else {secondNum += num;}
					} else {secondNum += num;}
				}
			document.getElementById('numTwo').innerHTML = secondNum;
		}
	}

	//keyboard version for getNums
	function getNumBoard(event) {
		//Takes the unicode value and converts to string for use
		var uniPress = event.which || event.keyCode;
		//add condition for enter keys
		if (uniPress == 13) {
			uniPress = "enter";
		}
		//Convert to string for checking actual values
		var keyValue = String.fromCharCode(uniPress);
		//1st Check if operator or number
		if((isNaN(keyValue) == false) || keyValue == '.') {
			//If value is a number, call getNums
			getNums(keyValue);
		} 
		//If operator, call getOps and moveNum
		else if(keyValue == '/' || keyValue == '*' || keyValue == '-' ||keyValue == '+') {
			getOps(keyValue);
			moveNum();
		}
		//If enter is pressed or equal (use unicode value to check), call doMath
		else if(uniPress == "enter" || keyValue == '=') {
			doMath();
		}
		// If backspace is pressed
		else if(uniPress == 127) {
			delNum();
		}
		else if(uniPress == 92) {
			eraseNums();
		}
	}



	function delNum() {
		var atNum = document.getElementById('currNum').innerHTML;
		// Clear doMath results if del called
		document.getElementById('numAns').innerHTML = null;
		if(atNum == 1) {
			document.getElementById('numOne').innerHTML = null;
		} else if(atNum == 2) {
			document.getElementById('numTwo').innerHTML = null;
			// Change currNum to 1
			document.getElementById('currNum').innerHTML = 1;
			}
	}

	function eraseNums() {
		document.getElementById('numOne').innerHTML = null;
		document.getElementById('numTwo').innerHTML = null;
		document.getElementById('numAns').innerHTML = null;
		document.getElementById('calcOp').innerHTML = "~";
		document.getElementById('currNum').innerHTML = 1;
	}

	function getOps(ops) {
	//incorporate negative values
	//if first number is not empty, proceed
	var atNum = document.getElementById('currNum').innerHTML;
	if(document.getElementById('numOne').innerHTML != ""){
		//if at second number and it's empty
		//make second number negative
		if(atNum == 2 && document.getElementById('numTwo').innerHTML == "") {
			document.getElementById('numTwo').innerHTML = "-";
		} else {
			document.getElementById('calcOp').innerHTML = ops;
			}
	} else {
			if(ops == '-'){
			document.getElementById('numOne').innerHTML = "-";
			}
		}

	}

	function moveNum() {
		//Move only if firstNum is not empty and does not contain "-"
		if(document.getElementById('numOne').innerHTML != "" && document.getElementById('numOne').innerHTML != "-") {
			document.getElementById('currNum').innerHTML = 2;
		}
		//Continuous calculations, reset numTwo and numAns
		if(document.getElementById('numAns').innerHTML != "") {
			document.getElementById('numOne').innerHTML = document.getElementById('numAns').innerHTML;
			document.getElementById('numTwo').innerHTML = null;
			document.getElementById('numAns').innerHTML = null;
		}
	}

	function doMath() {
		var calc = document.getElementById('calcOp').innerHTML;
		var x = Number(document.getElementById('numOne').innerHTML);
		var y = Number(document.getElementById('numTwo').innerHTML);

		if(calc == '+') {
			document.getElementById('numAns').innerHTML = x+y;
		} else if(calc == '-') {
			document.getElementById('numAns').innerHTML = x-y;
			} else if(calc == '*') {
			document.getElementById('numAns').innerHTML = x*y;
				} else if(calc == '/') {
			document.getElementById('numAns').innerHTML = x/y;
					}
	}

	function sqrtNum() {
		var x = Math.sqrt(Number(document.getElementById('numOne').innerHTML));
		document.getElementById('numAns').innerHTML = x;
	}